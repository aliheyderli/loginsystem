<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 5/3/2018
 * Time: 02:19
 */

class Database
{
    private $host = "localhost";
    private $db_name = "loginsystem";
    private $username = "root";
    private $password = "";
    private $type = "mysql";
    private $db = Null;

    public function __construct()
    {
        if($this->db == Null){
            try{
                $this->db = new PDO($this->type.':host='.$this->host.';dbname='.$this->db_name,$this->username,$this->password);
                $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            catch (PDOException $e){
                die($e->getMessage());
            }
        }

    }

    public function query($sql)
    {
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query;
    }

}